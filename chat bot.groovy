/*
______________________________

- Chat bot
Messaging with bot

Version: 1.0
PiApi Version: >0.0.4
_______________________________

 */


import cn.nukkit.Server
import cn.nukkit.event.player.PlayerChatEvent
import groovy.transform.Field
import tech.teslex.pi.PiApi
import tech.teslex.pi.annotations.PiOn

@Field autostart = true // exec on start

// Config:
prefix = "bot"
commands = [
		/online/              : { sender, match ->
			sender.sendMessage "${Server.instance.onlinePlayers.collect { it.value.name }.join(', ')}"
		},
		/killme/              : { sender, match ->
			sender.kill()
		},
		/.*help.*/            : { sender, match ->
			sender.sendMessage "whaaat???!"
		},
		/about ([a-zA-Z0-9]+)/: { sender, match ->
			player = Server.instance.getPlayer(match[0][1])

			sender.sendMessage """

${player.name} (${player.online ? '§aonline§r' : '§coffline§r'}): 
${player.x as int}, ${player.y as int}, ${player.z as int}
			
			""".trim()
		}
]


def cb = new ChatBotScript(commands: commands, prefix: prefix)
PiApi.registerEvents(cb)


class ChatBotScript {

	def prefix = "bot"

	def commands = []

	@PiOn
	def chat(PlayerChatEvent e) {
		def validResponse = commands.find { e.message ==~ "$prefix $it.key" }

		if (validResponse) {
			try {
				validResponse.value(e.player, e.message =~ "$prefix $validResponse.key")
			} catch (Exception ex) {
				e.player.sendMessage "[$prefix] §cError"
			}

			e.setCancelled(true)
		}
	}
}