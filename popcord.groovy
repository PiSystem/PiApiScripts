/*
______________________________

- Popcord
Show coordinates on popup

Version: 1.0
PiApi Version: >0.0.4
_______________________________

 */


import cn.nukkit.Player
import cn.nukkit.command.CommandSender
import cn.nukkit.event.player.PlayerMoveEvent
import groovy.transform.Field
import tech.teslex.pi.PiApi
import tech.teslex.pi.annotations.PiCommand
import tech.teslex.pi.annotations.PiOn

@Field autostart = true // exec on start


def ps = new PopcordScript()

PiApi.registerCommands(ps)
PiApi.registerEvents(ps)


class PopcordScript {

	def data = [] as Set

	def turn(Player player) {
		if (player in data)
			data.remove(player)
		else
			data.add(player)
	}

	@PiCommand(command = 'popcord')
	def popcord(CommandSender sender, s, String[] args) {
		if (!sender.player)
			return

		turn(PiApi.it.server.getPlayer(sender.name))
	}

	@PiOn
	def move(PlayerMoveEvent e) {
		if (e.player in data) {
			def x = e.player.x as int
			def y = e.player.y as int
			def z = e.player.z as int

			e.player.sendPopup("x: $x y: $y z: $z")
		}
	}
}
