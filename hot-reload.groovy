import cn.nukkit.Server
import cn.nukkit.scheduler.Task
import tech.teslex.pi.PiApi

path = "$Server.instance.dataPath${File.separator}scripts"
last_modified = (path as File).lastModified()

task = Server.instance.scheduler.scheduleRepeatingTask({
	last_modified_tmp = (path as File).lastModified()

	if (last_modified != last_modified_tmp) {
		last_modified = last_modified_tmp

		Server.instance.reload()
	}
} as Task, 1)

PiApi.onPluginDisable {
	// Server.instance.logger.info(task.taskId as String)
	Server.instance.scheduler.cancelTask(task.taskId)
}
